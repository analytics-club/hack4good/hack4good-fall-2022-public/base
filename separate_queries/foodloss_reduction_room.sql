-- foodloss_reduction_room
-- INPUT:
-- 		year, month, crop_id 
-- OUTPUT:
-- for a given crop_id make a table with farmers and rooms
-- and track baseline loss (user_farmer_commodity.quantity_below_market_price), 
-- weight of the crate and checkout loss in kg
select acm.cooling_unit_id, 
	acm.checkin_farmer, 
	sc.produce_id,
	acm.storage_crate_id, 
	acm.baseline_loss, 
	acm.weight as weight_in_kg, 
	acm.checkout_loss_in_kg as loss_in_kg
from analytics_crate_movements acm 
-- create a join path to storage_produce for filtering
join storage_crate sc on (acm.storage_crate_id = sc.id)
join storage_produce sp on (sc.produce_id = sp.id)
where (
	-- filter on time
	extract(year from acm.checkin_date) = %(year)s and
	extract(month from acm.checkin_date) = %(month)s
)
-- somehow order the queries
order by acm.cooling_unit_id, acm.checkin_farmer, sc.produce_id
