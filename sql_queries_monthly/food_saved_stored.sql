--This query returns the total amount of food stored for checkins in a given month per cooling unit as well as
-- the total amount of food saved. Food saved is calculated by subtracting total amount from losses reported
-- in the checkout survey

-- Sum over all the crate weights and subtract the reported loss (market survey) if it is not null. 
SELECT  acm.cooling_unit_id,
        sum(acm.weight) as kg_stored,
        sum(acm.weight - CASE WHEN acm.checkout_loss_in_kg is null THEN 0 ELSE acm.checkout_loss_in_kg END) as kg_saved
FROM analytics_crate_movements acm 
WHERE (
    extract(year from acm.checkin_date) = %(year)s AND
    extract(month from acm.checkin_date) = %(month)s
)
GROUP BY acm.cooling_unit_id


