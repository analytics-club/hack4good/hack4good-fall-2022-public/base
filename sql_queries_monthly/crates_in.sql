-- This query extracts the number of check ins performed in a particular month/year as well
-- as total number of crates checked in in that given month/year. Can be optimized by using analytics_crate_movements
-- view but the result is the same

WITH check_ins as (
    SELECT oc.id as check_in_id
    FROM operation_checkin oc,
        operation_movement om
    WHERE oc.movement_id = om.id
        and EXTRACT(
            MONTH
            FROM om.date
        ) = %(month)s
        and EXTRACT(
            YEAR
            FROM om.date
        ) = %(year)s
)
SELECT sc.cooling_unit_id,
    COUNT(*) as crates_checked_in_room,
    COUNT(DISTINCT sp.check_in_id) as check_in_operations_room
FROM storage_crate sc
    JOIN storage_produce sp on sc.produce_id = sp.id
WHERE sp.check_in_id in (
        SELECT check_in_id
        FROM check_ins
    )
GROUP BY sc.cooling_unit_id