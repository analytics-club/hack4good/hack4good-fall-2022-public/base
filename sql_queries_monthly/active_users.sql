-- This query returns the active cooling users and active female cooling users per cooling unit in the month
-- An active user is defined as someone who has at least one check in in the month for the cooling unit
-- This query takes all crates checken in for the given month/year, and takes distinct of user ids and user genders
-- to return the value

SELECT  acm.cooling_unit_id,
        count(DISTINCT(u.id)) as active_users,
        count(
            DISTINCT CASE
                WHEN u.gender = 'fe' THEN u.id
                else null
            END
        ) as active_users_female
FROM    analytics_crate_movements acm, 
        user_farmer uf,
        user_user u
WHERE 
        uf.id = acm.checkin_farmer
        AND u.id = uf.user_id
        AND EXTRACT(MONTH FROM acm.checkin_date) = %(month)s
        AND EXTRACT(YEAR FROM acm.checkin_date) = %(year)s
GROUP BY (acm.cooling_unit_id)