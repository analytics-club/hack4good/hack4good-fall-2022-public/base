-- This query extracts the number of check outs performed in a particular month/year as well
-- as total number of crates checked out in that given month/year. Can be optimized by using analytics_crate_movements
-- view but the result is the same

WITH check_outs as (
    SELECT oc.id as check_out_id
    FROM operation_checkout oc,
        operation_movement om
    WHERE oc.movement_id = om.id
        and EXTRACT(
            MONTH
            FROM om.date
        ) = %(month)s
        and EXTRACT(
            YEAR
            FROM om.date
        ) = %(year)s
)
SELECT sc.cooling_unit_id,
    COUNT(*) as crates_checked_out_room,
    COUNT(DISTINCT sc.check_out_id) as check_out_operations_room
FROM storage_crate sc
WHERE sc.check_out_id in (
        SELECT check_out_id
        FROM check_outs
    )
GROUP BY sc.cooling_unit_id