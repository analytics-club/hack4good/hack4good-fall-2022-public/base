-- This query returns the capacity for all stroage crates that were checked in during the given month/year
-- This means that the crate was checked in sometime before the end of the month as well as checked out only
-- after the end of the month

-- The query works by getting all crates not checked out / checked out after the end of the month
with relevant_checkouts as (
    SELECT sc.cooling_unit_id,
        sc.id as crate_id,
        om.date as checkout_date
    FROM storage_crate sc
        LEFT JOIN operation_checkout oc on(sc.check_out_id = oc.id)
        LEFT JOIN operation_movement om on (oc.movement_id = om.id)
    WHERE sc.check_out_id is null
        OR (
            EXTRACT(YEAR FROM om.date) = %(year)s
            AND EXTRACT(MONTH FROM om.date) >= %(month)s
            OR EXTRACT(YEAR FROM om.date) > %(year)s
        )
),
-- AND then filters those crates which were also checked in before the end of the month
relevant_checkins as (
    SELECT sc.cooling_unit_id,
        sc.id as crate_id,
        om.date as checkin_date
    FROM storage_crate sc
        JOIN storage_produce sp on(sc.produce_id = sp.id)
        JOIN operation_checkin oci on(sp.check_in_id = oci.id)
        JOIN operation_movement om on (oci.movement_id = om.id)
    WHERE sc.id IN (
            SELECT crate_id
            FROM relevant_checkouts
        )
        AND (
            EXTRACT(YEAR FROM om.date ) = %(year)s
            AND EXTRACT(MONTH FROM om.date) <= %(month)s
            OR EXTRACT(YEAR FROM om.date) < %(year)s
        )
)
SELECT rco.cooling_unit_id,
    crate_id,
    checkin_date,
    checkout_date,
    scu.capacity_in_number_crates
FROM relevant_checkouts rco
    JOIN relevant_checkins USING(crate_id)
    JOIN storage_coolingunit scu on (scu.id = rco.cooling_unit_id)
ORDER BY cooling_unit_id,
    checkin_date