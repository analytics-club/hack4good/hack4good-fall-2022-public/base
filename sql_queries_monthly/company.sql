-- This query retrieves company details per company id. The return values are:
-- + id, country AND name - retrieved FROM the user_company table
-- + total_registered_users (all AND only female) - retrieved FROM user_serviceprovider merged with the company table
-- + cooling_user_count (all AND female) - retrieved by linking the company to its operators AND any farmers linked to those operators
--       (the active user count is basically a subset of this only contianing users that performed a checkin)
-- + benificiary count - retrieved by multiplying the cooling user count by a fixed value - ie 3

-- company details getting base details FROM user_company
WITH company_details as (
    SELECT c.id as company_id,
        c.country,
        c.name,
        count(*) as total_registered_users,
        count(
            CASE
                WHEN u.gender = 'fe' then 1
            END
        ) as female_registered_users
    FROM user_company c,
        user_serviceprovider ru,
        user_user u
    WHERE c.id = ru.company_id
        AND ru.user_id = u.id
    GROUP BY c.id,
        c.country,
        c.name
),
-- separate cooling user temp table as this needs to group the farmers by company
company_cooling_users as (
    SELECT c.id as company_id,
        count(
            CASE
                -- to avoid the default cooling user
                WHEN u.phone != '' then 1
            END
        ) as cooling_user_count,
        count(
            CASE
                -- to avoid the default cooling user
                WHEN u.phone != ''
                AND u.gender = 'fe' then 1
            END
        ) as cooling_user_female_count,
        sum(
            CASE
                WHEN u.phone != '' then 3
                ELSE 0
            END
        ) as benificiary_count
    FROM user_company c,
        user_operator o,
        user_farmer f,
        user_user u
    WHERE c.id = o.company_id
        AND o.id = f.created_by_id
        AND u.id = f.user_id
    GROUP BY c.id
)
-- select required fields by combining above two tables
SELECT  cds.company_id,
        cds.country,
        cds.name,
        cds.total_registered_users,
        cds.female_registered_users,
        ccus.cooling_user_count,
        ccus.cooling_user_female_count,
        ccus.benificiary_count
FROM company_details cds
    JOIN company_cooling_users ccus USING(company_id)
ORDER BY cds.company_id