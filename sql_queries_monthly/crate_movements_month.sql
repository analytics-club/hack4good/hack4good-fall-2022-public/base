-- Extract storage crate movement information from the analytics crate movements view
-- for any crate that was either + CHECKED IN in the given month or
--                               + CHECKED OUT in the given month

-- Extracting both allows flexibility in the python computation as well as reduces number of sql queries performed

SELECT  storage_crate_id,
        cooling_unit_id,
        currency,
        weight,
        crop_id,
        checkin_id,
        checkin_date,
        checkin_operator,
        checkin_farmer,
        checkout_id,
        checkout_date,
        checkout_price,
        checkout_operator,
        checkout_loss_in_kg,
        checkout_survey_price,
        initial_survey_price
FROM analytics_crate_movements
WHERE 
    (EXTRACT(MONTH FROM checkin_date) = %(month)s AND EXTRACT(YEAR FROM checkin_date) = %(year)s)
    OR (checkout_date is not NULL AND (EXTRACT(MONTH FROM checkout_date) = %(month)s AND EXTRACT(YEAR FROM checkout_date) = %(year)s))
