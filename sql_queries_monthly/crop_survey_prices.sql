-- This query gets the overall average price of crops sold from a farmer (so per farmer per crop) from all their
-- market survey responses as well as the initial survey price.

SELECT  checkin_farmer as farmer_id,
        crop_id,
        AVG(checkout_survey_price) as average_checkout_survey_price,
        AVG(initial_survey_price) initial_survey_price
FROM analytics_crate_movements
WHERE checkout_survey_price is not NULL
GROUP BY (checkin_farmer, crop_id)
