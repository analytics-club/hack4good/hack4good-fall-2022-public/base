# BASE

## Description

This repository is the result of the Hack4Good project working with the NGO BASE. It is a data collection program for the BASE Coldtivate App and allows for the automatic generation of a report containing key indicators about the coldrooms that Coldtivate monitors.

## Prerequisites

- Python version to be updated _here_.

Inside your virtual environment `h4gvenv` run:

```
> h4gvenv -m pip install -r requirements.txt
```

With POSIX and bash/zsh to create and activate a virtual environment run:

```
> python3 -m venv h4gvenv
> source h4gvenv/bin/activate
```

## Use

### Report Execution
This codebase can be used to either generate the indicator output csv based on the google sheet linked below or for running and saving the output of additional queries. This will output a csv with one row per cooling unit and the required indicators.
In order to generate the indicator output csv file, run the command below. A date must be specified. The output is saved to the folder `outputs` with the corresponding year and month it was run for.

```
python3 indicator_reporter.py --month MONTH --year YEAR
```

If additional queries should be saved as reports, it can be run with the second command. Currently this creates only 1 new csv - called foodloss_reduction_room which gives the food loss statistics but per cooling unit, per user, per crop and is not grouped by cooling unit.
The output is saved to the folder `outputs\individual\` with the corresponding year and month it was run for.

```
python3 separate_queries_reporter.py --month MONTH --year YEAR
```

### Report Extension
In order to add additional indicators to the output report, the following steps can be taken:

#### Adding new queries to extract data
Any sql file in the 'sql_queries_monthly' are automatically executed and saved to a dictionary. If you need new queries, then just add a new sql file to that folder.

#### Adding new computation in python
New computation methods can be added to the dataProcessor.py file as functions. They can then be called in the 'additional_processing' function in the indicator_reporter.py file passing in any of the queried dataframes or arguments as inputs

#### Merging in new indicators to the output dataframe
The dataframes finally merge is controlled by the `queries_data.json` file. If you have added a new indicator to an existing dataframe it should get merged in into the output automatically. 
If you have added a new dataframe (either via a new query or new computation) you can specify how it is to be merged in the json file and that allow it to be merged and present in the output.


### View Creation
We currently implemented a view 'analytics_crate_movements' that helps combine all the checkin and checkout information into one view in order to make future queries simpler. If you need to edit this to add new fields, then the view can be recreated by editing the corrsponding file and running the following command

```
python3 create_view.py -v analytics_crate_movements
```
*Note: In case you face errors, you might have to drop the view first and then recreate it.

If you want to add new views, just add their definition to the 'views' folder as a separate sql file. Calling the same command above but with the new sql filename will create the new view.


## Team members

- Ambarish Prakash
- Fredrik Nestaas
- Lucien Walewski
- Shangen Li

## Individual query description
Each query has a description of what it extracts as a comment on the top of the file. For more information on how its used, refer to the Indicator mapping table to see which indicators are based on it


## Indicator mappings
This table mentions for each indicator on the output csv, which queries it gets its data from and what processing (if any) it undergoes.

| Indicator Description | Queries Based off | Computation Used |
| --- | --- | --- |
| cooling_unit_id | cooling_unit | -- |
| state | cooling_unit | -- |
| company_id | cooling_unit | -- |
| cooling_unit_type | cooling_unit | -- |
| capacity_in_metric_tons | cooling_unit | -- |
| food_capacity_in_metric_tons | cooling_unit | -- |
| capacity_in_number_crates | cooling_unit | -- |
| operators | cooling_unit | -- |
| operators_female | cooling_unit | -- |
| pricing_strategy | cooling_unit | -- |
| country | company | -- |
| name | company | -- |
| total_registered_users | company | -- |
| female_registered_users | company | -- |
| cooling_user_count | company | -- |
| cooling_user_female_count |company--- | -- |
| benificiary_count | company | -- |
| active_users | active_users | -- |
| active_users_female | active_users | -- |
| crates_checked_in_room | crates_in | -- |
| check_in_operations_room | crates_in | -- |
| crates_checked_out_room | crates_out | -- |
| check_out_operations_room | crates_out | -- |
| occupancy_room | crate_movement_month | occupancy_room_computation |
| revenue_room | checkouts in crate_movement_month | revenue_room_computation |
| revenue_room_usd | checkouts in crate_movement_month | revenue_room_computation |
| max_surplus_room | checkouts in crate_movement_month | max_surplus_room_computation |
| max_surplus_room_usd | checkouts in crate_movement_month | max_surplus_room_computation |
| sellingprice_increase_room | checkouts in crate_movement_month | selling_price_increase_computation |
| survey_filled_room | checkouts in crate_movement_month | survey_filled_computation |

If there any questions in the indicators you can check the following resources:
- This [google sheet](https://docs.google.com/spreadsheets/d/1fzTH00M7_0DB18lbSxvuZ3bR3Smsv46y/edit#gid=546324656) here contains the logic behind all the different indicators.
- The sql queries for the data for each indicator can be found in the `sql_queries_monthly` folder and each query should have information on what it extracts
- All processing is done in the `dataProcessor.py` and each method has documentation on what it is called with and what it does and returns

