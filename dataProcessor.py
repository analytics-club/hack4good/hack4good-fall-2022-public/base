import pandas as pd
import calendar
import datetime as dt
import yfinance as yf
import pytz


class DataProcessor():
    """
    Class to process data provided by SQL queries
    """
    @staticmethod
    def occupancy_room_computation(
        table: pd.DataFrame,
        year: int,
        month: int,
    ):
        """
        Do the computation part of the occupancy_room query
        Parameters:
            table:
                DataFrame containing the columns 'cooling_unit_id', 'crate_id', 'checkin_date', 'checkout_date', 'capacity_in_number_crates'
        Returns:
            the mean occupancy for the table per cooling_unit_id
        """
        days_month = calendar.monthrange(
            year, month)[-1]  # number of days in a month
        first_day_of_month = dt.datetime(
            year=year, month=month, day=1, tzinfo=pytz.UTC)
        last_day_of_month = first_day_of_month + dt.timedelta(days=days_month)

        df = table.copy()
        checkout = df['checkout_date'].fillna(last_day_of_month)
        # we are only concerned about this month
        checkout = checkout.clip(upper=last_day_of_month)

        checkin = df['checkin_date'].clip(lower=first_day_of_month)

        # for each crate, how long was it checked in
        df['time_span'] = checkout - checkin

        weight = df['time_span'].dt.total_seconds(
        ) / dt.timedelta(days=days_month).total_seconds()
        # over a month, what fraction of the month was the crate present?
        df['occupancy'] = weight

        # aggregate the crates
        capacity = df[['cooling_unit_id', 'capacity_in_number_crates']].groupby(
            'cooling_unit_id').median()
        occupancy = df[['cooling_unit_id', 'occupancy']
                       ].groupby('cooling_unit_id').sum()

        mean_occupancy = occupancy['occupancy'] / \
            capacity['capacity_in_number_crates']

        frac = mean_occupancy.rename(
            'occupancy_room', inplace=False)  # occupancy as a fraction
        return 100*frac.round(2)  # percentage with 2 decimal places

    @staticmethod
    def revenue_room_computation(checkouts: pd.DataFrame):
        """
            Compute the revenue (sum of money collected for each checkout) of the room
            both in the local currency and in USD
            Parameters:
            table:
                DataFrame of create checkouts with the columns 'cooling_unit_id', 'checkout_date', 'checkout_price', 'currency'
            Returns:
                The revenue in the given currency and in USD for each cooling unit room
        """
        pd.set_option('mode.chained_assignment', None)

        # create dictonary of currencies and exchange rates
        rates = {}
        currencies = checkouts['currency'].unique()
        for currency in currencies:
            curr_to_usd_rate = yf.Ticker(currency+'=X').info['previousClose']
            rates[currency] = 1 / curr_to_usd_rate
        checkouts.loc[:, 'usd_exchange_rate'] = checkouts['currency'].map(
            lambda x: rates[x])

        # calculate price in usd using exchange rates
        checkouts = checkouts.assign(
            revenue_room_usd=lambda x: x['checkout_price']*x['usd_exchange_rate'])

        # group the revenue and revenue usd per room and return
        checkouts.loc[:, 'revenue_room'] = checkouts['checkout_price']
        revenue_df = checkouts[['cooling_unit_id', 'revenue_room',
                                'revenue_room_usd']].groupby('cooling_unit_id').sum()
        revenue_df['revenue_room'] = revenue_df['revenue_room'].round(2)
        revenue_df['revenue_room_usd'] = revenue_df['revenue_room_usd'].round(
            2)

        return revenue_df

    @staticmethod
    def max_surplus_room_computation(checkouts: pd.DataFrame):
        """
            Compute the max_surplus (checkout_survey_price * (weight - checkout_loss_in_kg) - checkout_price) of the room
            both in the local currency and in USD
            Parameters:
            table:
                DataFrame of create checkouts with the columns 'cooling_unit_id', 'checkout_date', 'checkout_price', 'currency', checkout_survey_price
            Returns:
                The revenue in the given currency and in USD for each cooling unit room
        """
        pd.set_option('mode.chained_assignment', None)

        # remove crates that dont have a checkout survey price yet
        checkouts = checkouts[checkouts['checkout_survey_price'].notnull()]

        # If checkout_survey_price is not missing but checkout_loss_in_kg is, then assume no loss
        checkouts['checkout_loss_in_kg'].fillna(0, inplace=True)

        # create dictonary of currencies and exchange rates
        rates = {}
        currencies = checkouts['currency'].unique()
        for currency in currencies:
            curr_to_usd_rate = yf.Ticker(currency+'=X').info['previousClose']
            rates[currency] = 1 / curr_to_usd_rate
        checkouts.loc[:, 'usd_exchange_rate'] = checkouts['currency'].map(
            lambda x: rates[x])

        # group the max_surplus and max_surplus usd per room and return

        checkouts['fee'] = checkouts['checkout_price']
        checkouts['selling_revenue'] = checkouts['checkout_survey_price'] * \
            (checkouts['weight'] - checkouts['checkout_loss_in_kg'])
        checkouts['max_surplus_room'] = checkouts['selling_revenue'] - \
            checkouts['fee']
        checkouts = checkouts.assign(
            max_surplus_room_usd=lambda x: x['max_surplus_room']*x['usd_exchange_rate'])

        surplus_df = checkouts[['cooling_unit_id', 'max_surplus_room',
                                'max_surplus_room_usd']].groupby('cooling_unit_id').sum()
        surplus_df['max_surplus_room'] = surplus_df['max_surplus_room'].round(
            2)
        surplus_df['max_surplus_room_usd'] = surplus_df['max_surplus_room_usd'].round(
            2)

        return surplus_df

    @staticmethod
    def selling_price_increase_computation(
        checkouts: pd.DataFrame,
        crop_prices: pd.DataFrame
    ):
        """
            Compute the selling price change (percentage change of checkout survey price to original survey price) 
            of the room for all checkouts in the room for the month with data present (initial and checkout survey)
            Parameters:
                checkouts:
                    DataFrame of crate checkouts with relevant columns 'cooling_unit_id', 'checkout_date', 'checkin_farmer', 'crop_id'
                crop_prices:
                    DataFrame of farmer crop prices with relevant columns 'farmer_id', 'crop_id', 'average_checkout_survey_price' and 'initial_survey_price'
            Returns:
                The selling price increase for all VALID checkouts for give month averaged per room (A valid 
                checkout is one that has both an average checkout survey price and initial survey price)
        """
        pd.set_option('mode.chained_assignment', None)

        # rename checkin_farmer to farmer_id for the join
        checkouts['farmer_id'] = checkouts['checkin_farmer']

        # filter out crop prices that dont have the initial survey price filled in (aka remove invalid ones)
        crop_prices = crop_prices[crop_prices.initial_survey_price.notnull()]
        crop_prices['initial_survey_price'] = crop_prices['initial_survey_price'].astype(
            float)
        crop_prices['sellingprice_increase_room'] = (
            crop_prices['average_checkout_survey_price'] - crop_prices['initial_survey_price'])
        crop_prices['sellingprice_increase_room'] = (
            crop_prices['sellingprice_increase_room'] / crop_prices['initial_survey_price'] * 100).round(2)

        # join the two tables
        checkouts = pd.merge(checkouts, crop_prices, how='left', on=[
                             'farmer_id', 'crop_id'])

        # group by colling_unit_id and return average
        selling_price_df = checkouts[[
            'cooling_unit_id', 'sellingprice_increase_room']].groupby('cooling_unit_id').mean()

        return selling_price_df

    @staticmethod
    def survey_filled_computation(
        checkouts: pd.DataFrame
    ):
        """
            Compute the number of checkout surveys filled per room. A checkout survey is considered filled if the 
            checkout survey price value is not empty
            Parameters:
                checkouts:
                    DataFrame of crate checkouts for the month with relevant columns 'cooling_unit_id', 'checkout_id' and 'checkout_survey_price'
            Returns:
                survey_filled_df:
                    A df consisting of the count of filled surveys per room 
        """
        pd.set_option('mode.chained_assignment', None)

        # take only the checkouts that has the checkout survey price filled
        checkouts = checkouts[checkouts.checkout_survey_price.notnull()]

        # take only distict checkouts (marked by distinct checkout ids) and
        # group by cooling unit by counting the grouped rows and return
        checkouts['survey_filled_room'] = checkouts['checkout_id']
        survey_filled_df = checkouts[['cooling_unit_id', 'survey_filled_room']].drop_duplicates(
            ).groupby('cooling_unit_id').count()

        return survey_filled_df
