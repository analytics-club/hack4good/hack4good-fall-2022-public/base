-- A view that provides a summarized result of all storage crates
-- For each crate it returns information about it self (crop, weight, price, etc)
-- it's checkin movements (such as checkin date, operator, farmer that checked it in, etc)
-- as well as checkout movement information (checkout date, operator, checkout survey loss and price, etc)


CREATE OR REPLACE VIEW analytics_crate_movements AS
    -- Create a temp table for the checkout information as it needs to join storage crates with checkout movements
    WITH checkout_info as (
        select  sc.id, 
                sc.check_out_id as checkout_id,
                om.date as checkout_date,
                cko.price as checkout_price,
                om.operator_id as checkout_operator,
                ms.loss_in_kg as checkout_loss_in_kg,
                ms.price as checkout_survey_price
        from storage_crate sc
        left outer join operation_checkout cko on (sc.check_out_id = cko.id)
        left outer join operation_movement om on (cko.movement_id = om.id)
        left outer join operation_marketsurvey ms on (ms.checkout_id = cko.id)
    )
    -- Join storage crate with the checkin movements to get the checkin movement information. Additionally
    -- join that with the checkout information on the storage crate id to get a combined view of both checkin and
    -- checkout information for each storage crate
    select  sc.id as storage_crate_id,
            sc.cooling_unit_id,
            sc.currency,
            sc.weight,
            sp.crop_id,
            cin.id as checkin_id,
            om.date as checkin_date,
            om.operator_id as checkin_operator,
            cin.farmer_id as checkin_farmer,
            cko.checkout_id,
            cko.checkout_date,
            cko.checkout_price,
            cko.checkout_operator,
            cko.checkout_loss_in_kg,
            cko.checkout_survey_price,
            fsc.average_price as initial_survey_price,
            fsc.quantity_below_market_price as baseline_loss
    from storage_crate sc
        left join checkout_info cko on (sc.id = cko.id)
        left join storage_produce sp on (sc.produce_id = sp.id)
        left join operation_checkin cin on (sp.check_in_id = cin.id)
        left join operation_movement om on (cin.movement_id = om.id)
        left join user_farmersurvey fs on (fs.farmer_id = cin.farmer_id)
        left join user_farmersurveycommodity fsc on (fsc.farmer_survey_id = fs.id and fsc.crop_id = sp.crop_id)
