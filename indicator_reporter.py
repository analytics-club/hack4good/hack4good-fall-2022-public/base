import pandas as pd
from dotenv import load_dotenv
import argparse
from utils import *
from dataProcessor import DataProcessor


def parse_arguments():
    parser = argparse.ArgumentParser(description='Run report for specific month and year')
    parser.add_argument('--month', type=int, help='the month for when to run the report for')
    parser.add_argument('--year', type=int, help='the year for when to run the report for')
    return parser.parse_args()

def execute_query(conn: connection, query_path: str, params: dict) -> pd.DataFrame:
    """Given a connection object, the path towards an SQL file and a
    dictionary containing the optional parameter values, execute
    the query and return the result in a DataFrame"""
    cursor = conn.cursor()
    file_name = os.path.join(os.path.dirname(__file__), query_path)
    with open(file_name, 'r') as fp:
        try:
            cursor.execute(fp.read(), params)
            df = pd.DataFrame(cursor.fetchall())
            df.columns = [desc[0] for desc in cursor.description]
        finally:
            cursor.close()
    return df

def additional_processing(dfs: dict, args: dict) -> None:
    """
    Run additional computation for some of the more complicated indicators. Results are saved back
    to the input dictionary. For more information on the processed columns, look at the relevant functions
    in the DataProcessor
    """
    # Crate movements month includes all movements (either check in or check out) for the given month
    # Many of the computations require only checkouts, so filter movements for only checkouts in the month
    crate_movements = dfs['crate_movements_month']
    crate_movements['checkout_month'] = pd.to_datetime(crate_movements['checkout_date']).dt.month
    crate_movements['checkout_year'] = pd.to_datetime(crate_movements['checkout_date']).dt.year
    checkouts = crate_movements[(crate_movements['checkout_month'] == args['month']) & 
                                (crate_movements['checkout_year'] == args['year'])]
    
    # Additional computation to calculate occupancy_room
    dfs['occupancy_room'] = DataProcessor.occupancy_room_computation(dfs['occupancy_room'],
                            year=args['year'], month=args['month'])

    # Additional computation to calculate revenue_room
    dfs['revenue_room'] = DataProcessor.revenue_room_computation(checkouts)

    # Additional computation to calculate sellingprice increase room
    dfs['sellingprice_increase_room'] = DataProcessor.selling_price_increase_computation(
                            checkouts, dfs['crop_survey_prices'])

    # Additional computation to calculate sellingprice increase room
    dfs['survey_filled_room'] = DataProcessor.survey_filled_computation(checkouts)

    # Additional computation to calculate max_surplus_room
    dfs['max_surplus_room'] = DataProcessor.max_surplus_room_computation(checkouts)


def extract_data(
    conn: connection, 
    args: dict, 
    queries_data: dict, 
    dir: str='sql_queries_monthly',
    post_process: bool=True, 
    merge: bool=True
) -> pd.DataFrame:
    """
    Run all the queries in the directory
    dir before merging all the
    returned dataframes into a single dataframe
    which is returned as the output
    Parameters:
        conn:
            connection object to DB
        args:
            arguments from argparse. Must contain 'year' and 'month'
        queries_data:
            dict containing information on how to merge dataframes, and nan-imputation
        dir:
            The directory under which the queries are saved
        post_process:
            Whether to run post-processing after executing the queries
        merge:
            Whether to merge the queries. If True, return one large dataframe, 
            otherwise, return a dict of the query results from the .sql files
            found in dir, where the key is the query name
    Returns:
        Query results as a dataframe if merge, and otherwise as a dict of dataframes

    """

    dfs = {}  # Dictionary of query names, query result key-value pairs
    # Iterate over query files, run the queries and store the result
    for file_name in os.listdir(dir):
        query_name = file_name.replace('.sql', '')
        query_path = os.path.join(dir, file_name)
        dfs[query_name] = execute_query(conn, query_path, vars(args))

    # Perform additional computation for some of the indicators
    if post_process:
        additional_processing(dfs, vars(args))

    if merge:
        # Merge the dataframes
        merged_df = dfs['cooling_unit'] # Use the cooling unit df as the base

        # For each of the dfs to merge in the config, merge it with the base df
        for df_to_merge in queries_data['dfs_to_merge']:
            df = dfs[df_to_merge['name']]
            merged_df = pd.merge(merged_df, df, on=df_to_merge['on'], how='left')

        return merged_df
    else:
        return dfs


def make_conn_db():
    """
    Method to connect to the database
    Reads DB_USERNAME and DB_PASSWORD from .env and uses these as credentials
    Returns:
        a connection object which is connected to the database
    """
    load_dotenv()
    return setup_connection()


def main():
    args = parse_arguments()
    queries_data = load_json_variables('queries_data.json')
    conn = make_conn_db()
    try:
        indicator_df = extract_data(conn, args, queries_data)
        replace_nans(indicator_df, queries_data['zero_na_columns'])
        indicator_df.to_csv(f'outputs/out_{args.year}_{args.month}.csv', index=False)
    finally:
        if (conn):
            conn.close()


if __name__ == "__main__":
    main()
