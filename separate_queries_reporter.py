from indicator_reporter import extract_data, make_conn_db, parse_arguments

"""
Some files are not appropriate to put into the monthly report.
This script enables you to run individual queries and save them
individually. To that end, save the queries under a folder 
'separate_queries', and the results are saved to 'outputs/individual'.
"""

def main():
    conn = make_conn_db()
    args = parse_arguments()
    queries_data = {} # we neither fill nans nor merge these dfs
    data = extract_data(conn, args, queries_data, dir='separate_queries', post_process=False, merge=False)
    for name, df in data.items():
        # save query result to CSV in outputs/individual
        df.to_csv(f'outputs/individual/{name}_{args.year}_{args.month}.csv')

if __name__ == '__main__':
    main()